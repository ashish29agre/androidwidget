package in.ashish29agre.homewidget;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class HomeWidgetActivity extends Activity implements OnClickListener {
	private int counter;
	private ImageView widgetImageView;
	private Button widgetButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_widget);
		counter = 0;
		widgetImageView = (ImageView) findViewById(R.id.widget_image);
		widgetButton = (Button) findViewById(R.id.widget_button);
		widgetButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		widgetImageView.setImageResource(++counter % 2 == 0 ? R.drawable.me
				: R.drawable.wordpress_icon);
	}

}
