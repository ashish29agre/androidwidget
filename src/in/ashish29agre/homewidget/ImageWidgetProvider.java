package in.ashish29agre.homewidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class ImageWidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				R.layout.activity_home_widget);
		remoteViews.setOnClickPendingIntent(R.id.widget_button,
				buildButtonPendingIntent(context));
		pusgWidgetUpdate(context, remoteViews);
	}

	public static void pusgWidgetUpdate(Context context, RemoteViews remoteViews) {
		ComponentName componentName = new ComponentName(context,
				ImageWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(componentName, remoteViews);
	}

	public static PendingIntent buildButtonPendingIntent(Context context) {
		Intent intent = new Intent();
		intent.setAction("pl.looksok.intent.action.CHANGE_PIECTURE");
		return PendingIntent.getBroadcast(context, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}
}
