/**
 * 
 */
package in.ashish29agre.homewidget.receiver;

import java.util.logging.Logger;

import in.ashish29agre.homewidget.ImageWidgetProvider;
import in.ashish29agre.homewidget.R;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * @author ashish
 * 
 */
public class WidgetIntentReceiver extends BroadcastReceiver {
	private static int counter = 0;
	private static Logger log = Logger.getLogger(WidgetIntentReceiver.class
			.getName());

	@Override
	public void onReceive(Context context, Intent intent) {
		log.info("OnReceive");
		if (intent.getAction().equals(
				"pl.looksok.intent.action.CHANGE_PIECTURE")) {
			log.info("Equals Update");
			updateWidget(context);
		}
	}

	private void updateWidget(Context context) {
		log.info("UpdateWidget");
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				R.layout.activity_home_widget);
		remoteViews.setImageViewResource(R.id.widget_image,
				updateRemoteImageView());
		remoteViews.setOnClickPendingIntent(R.id.widget_button,
				ImageWidgetProvider.buildButtonPendingIntent(context));
		ImageWidgetProvider.pusgWidgetUpdate(context, remoteViews);
	}

	private int updateRemoteImageView() {
		++counter;
		return counter % 2 == 0 ? R.drawable.me : R.drawable.wordpress_icon;
	}
}
