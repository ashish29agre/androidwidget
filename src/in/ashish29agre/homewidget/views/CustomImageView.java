/**
 * 
 */
package in.ashish29agre.homewidget.views;

import java.util.Calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

/**
 * @author ashish
 * 
 */
public class CustomImageView extends ImageView implements Runnable {

	public CustomImageView(Context context) {
		super(context);
		init();
	}

	public CustomImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private String text;
	private Paint paint;
	private Handler handler;

	private void init() {
		paint = new Paint();
		text = "Size";
		handler = new Handler();
		handler.postDelayed(this, 1000);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// super.onDraw(canvas);
		canvas.drawText(text, 50, 50, paint);
	}

	@Override
	public void run() {
		text = Long.toString(Calendar.getInstance().getTimeInMillis());
		handler.postDelayed(this, 1000);
	}
}
